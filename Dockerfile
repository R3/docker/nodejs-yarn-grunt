FROM node

COPY . /app
WORKDIR /app

ENV PIP_BREAK_SYSTEM_PACKAGES 1

RUN apt-get update && \
    apt-get install -yqq python3 python-is-python3 python3-pip npm jq git-lfs && \
    npm install -g grunt-cli && \
    echo '{ "allow_root": true }' > /root/.bowerrc && \
    wget https://gitlab.lcsb.uni.lu/R3/outreach/templates/presentation/raw/master/requirements.txt && \
    pip install -r requirements.txt

# Define working directory.
WORKDIR /presentation

EXPOSE 9000

# Define default command.
CMD ["bash"]

